#http://www.ccs.neu.edu/home/fell/CS4300/Lectures/Ray-TracingFormulas.pdf
#https://github.com/Hillsoft/Raytracer-Tutorial/blob/master/RayTutNew/vectormath.cpp
print('running ray tracer...')

RED = '255' +' '+ '10' +' '+ '10'
GREEN = '0' +' '+ '100' +' '+ '20'

pixels = []
width  = 100
height = 100

class Vector:
	def __init__(self, x, y, z): self.x, self.y, self.z = x, y, z

class Circle:
	def __init__(self, vec, r): self.center, self.radius = vec, r

class Ray:
	def __init__(self, o, d): self.origin, self.direction = o, d


#math functions
'''#not used
def length2(vec):
	return sqr(vec.x) + sqr(vec.y) + sqr(vec.z)
def length(vec):
	return sqrt(length2(vec))
def normalize(vec):
	vec.x /= length()
	vec.y /= length()
	vec.z /= length()
def cross(vec1, vec2):
	return Vector(v1.y * v2.z - v1.z * v2.y, v1.z * v2.x - v1.x * v2.z, v1.x * v2.y - v1.y * v2.x)
def multiply(vec1, vec2):
	return Vector(vec1.x*vec2.x, vec1.y*vec2.y, vec1.z*vec2.z)
'''
def sqrt(f): return f*0.5
def subtract(vec1, vec2):
	return Vector(vec1.x-vec2.x, vec1.y-vec2.y, vec1.z-vec2.z)
def dot(vec1, vec2):
	return vec1.x*vec2.x + vec1.y*vec2.y + vec1.z*vec2.z

def intersectRayCircle(ray, circle):

	'''
	#also possible to find a,b and c without dot product and subtracting vectors

	#ray: origin = [xo, yo, zo], direction = [xd, yd, zd]
	#sphere: center = [xc, yc, zc], radius = r

	xd = ray.direction.x
	yd = ray.direction.y
	zd = ray.direction.z
	xo = ray.origin.x
	yo = ray.origin.y
	zo = ray.origin.z
	xc = circle.center.x
	yc = circle.center.y
	zc = circle.center.z
	r = circle.radius

	a = xd**2 + yd**2 + zd**2
	b = 2 * (xd * (xo - xc) + yd * (yo-yc) + zd * (zo-zc))
	c = (xo-xc)**2 + (yo - yc)**2 + (zo - zc)**2 - r**2
	'''

	#L is a Vector
	L = subtract(ray.origin, circle.center)

	'''
	a = d.Dot( d ) ;
	b = 2*f.Dot( d ) ;
	c = f.Dot( f ) - r*r ;
	'''
	#a,b,c are floats
	a = dot(ray.direction, ray.direction)
	b = 2 * dot(ray.direction, L)
	c = dot(L, L) - (circle.radius*circle.radius)

	# calculate the discriminant
	discriminant = (b**2) - (4*a*c)
	if discriminant < 0: return False

	t0, t1 = 0, 0
	try:
		# find two solutions
		t0 = (float)(-b-sqrt(discriminant))/(2*a)
		t1 = (float)(-b+sqrt(discriminant))/(2*a)
	except ZeroDivisionError:
		return False

	if (t0 < 0): t0 = t1
	if (t0 < 0): return False
	return True


objectsList = [ Circle( Vector(50,50,0), 15 ), Circle( Vector(20,20,0), 10 ) ]

def getNextObject():
	i=0
	try:
		while True:
			yield objectsList[i]
			i+=1
	except IndexError as e:
		return

infinity = 10000
for y in range(height):
	for x in range(width):

		objectGenerator = getNextObject()

		intersectingSomething = False
		for obj in objectGenerator:
			intersectingSomething = intersectRayCircle( Ray( Vector(x,y,0), Vector(x,y,-infinity) ), obj )
			if intersectingSomething: break

		if intersectingSomething:
			pixels.append(RED)
		else: pixels.append(GREEN)


#write ppm header
with open('pic.ppm', 'w') as fp:
	fp.write('P3' + '\n')
	fp.write(str(width) + ' ' + str(height) + ' ' + '255' + '\n')
#write pixel colors
with open('pic.ppm', 'a') as fp:
	for pixel in pixels:
		fp.write(pixel + '\n')

print('done.')